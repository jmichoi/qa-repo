"""
==============
 Instructions
==============

Installing dependencies
1. Open terminal
2. Check if you need to install pip: (type in terminal) `python -m pip --version`
3. `sudo python -m ensurepip`
4. `python3 -m pip install wheel`
5. `python3 -m pip install pandas`

Where to place this file
6. Make sure your csv file is in the same folder as `search_code.py`
7. Open `search_code.py` and replace value in file_path

Splitting big CSV file into chunks
8. Change indices of rows to look at for each run.
    Example: Splitting by chunks of 100 rows each
        First run
            row_num_to_start = 0
            row_num_to_end = 100
        Second run
            row_num_to_start = 100 (row 100 wasn't read in first run)
            row_num_to_end = 200
        And so forth.
    If you want to go to the end of the file, set:
        row_num_to_end = len(dataframe)

Running this file
9. In terminal: `python3 search_code.py`
10. Google Chrome will pop up with searches of unique instances of "[Contractor name] [City]" in as many tabs as there are unique searches.
"""

import os
import pandas as pd

filepath = 'Sample_entity_id_data2.csv' # to replace

dataframe = pd.read_csv(
    filepath,
    usecols=['NAME','SOLDTOCITY']
)

row_num_to_start = 0 # to replace
row_num_to_end = 100 # to replace

searches_to_make = set()
for i in range(row_num_to_start, row_num_to_end):
    name = dataframe.values[i][0]
    city = dataframe.values[i][1]
    searches_to_make.add(f'{name} {city}')
print(searches_to_make)

for search_query in searches_to_make:
    os.system(f"open -a \'Google Chrome\' \'http://www.google.com/search?q={search_query}\'")
